﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebAPIDesafio.Models
{
    public class WebAPIDesafioContext : DbContext
    {
        public WebAPIDesafioContext (DbContextOptions<WebAPIDesafioContext> options)
            : base(options)
        {
        }

        public DbSet<WebAPIDesafio.Models.Solicitud> Solicitud { get; set; }
    }
}
