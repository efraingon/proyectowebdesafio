﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIDesafio.Models
{



    public enum CasaMagica { Gryffindor, Hufflepuff, Ravenclaw, Slytherin };
    
    public class Solicitud
    {
       

        public int Id {get; set; }

        [Required]
        [StringLength(20)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(20)]
        public string Apellido { get; set; }

        [Range(0, 9999999999, ErrorMessage = "identificacion invalidad")]
        public decimal  Identificacion { get; set; }

        [Required]
        [Range(0, 99, ErrorMessage = "edad hasta los 99 años")]

        public int  Edad { get; set; }

        [EnumDataType(typeof(CasaMagica))]
        public CasaMagica? Casa { get; set; }
        public string CasaString { get; set; }

       


    }


}
