using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPIDesafio.Models;

namespace WebAPIDesafio.Controllers
{
    [Produces("application/json")]
    [Route("api/Solicituds")]
    public class SolicitudsController : Controller
    {
        private readonly WebAPIDesafioContext _context;

        public SolicitudsController(WebAPIDesafioContext context)
        {
            _context = context;
        }

        // GET: api/Solicituds
        [HttpGet]
        public IEnumerable<Solicitud> GetSolicitud()
        {

            var solicituds = _context.Solicitud.OrderBy(m => m.Id);


            foreach (var solicitud in solicituds)
            {

                switch (solicitud.Casa)
                {
                    case CasaMagica.Gryffindor:
                        solicitud.CasaString = Convert.ToString(CasaMagica.Gryffindor);
                        break;
                    case CasaMagica.Hufflepuff:
                        solicitud.CasaString = Convert.ToString(CasaMagica.Hufflepuff);
                        break;

                    case CasaMagica.Ravenclaw:
                        solicitud.CasaString = Convert.ToString(CasaMagica.Ravenclaw);
                        break;
                    case CasaMagica.Slytherin:
                        solicitud.CasaString = Convert.ToString(CasaMagica.Slytherin);
                        break;
                    default:
                        solicitud.CasaString = Convert.ToString(CasaMagica.Gryffindor);

                        break;

                }

              
                    _context.Solicitud.Update(solicitud);

           
            }



            return _context.Solicitud;


        }



        // GET: api/Solicituds/5
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetSolicitud([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var solicitud = await _context.Solicitud.SingleOrDefaultAsync(m => m.Id == id);

            if (solicitud == null)
            {
                return NotFound();
            }

            return Ok(solicitud);
        }

        // PUT: api/Solicituds/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSolicitud([FromRoute] int id, [FromBody] Solicitud solicitud)
        {


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != solicitud.Id)
            {
                return BadRequest();
            }

            switch (solicitud.Casa)
            {
                case CasaMagica.Gryffindor:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Gryffindor);
                    break;
                case CasaMagica.Hufflepuff:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Hufflepuff);
                    break;

                case CasaMagica.Ravenclaw:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Ravenclaw);
                    break;
                case CasaMagica.Slytherin:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Slytherin);
                    break;
                default:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Gryffindor);

                    break;

            }



            _context.Entry(solicitud).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SolicitudExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Solicituds
        [HttpPost]
        public async Task<IActionResult> PostSolicitud([FromBody] Solicitud solicitud)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            switch (solicitud.Casa)
            {
                case CasaMagica.Gryffindor:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Gryffindor);
                    break;
                case CasaMagica.Hufflepuff:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Hufflepuff);
                    break;

                case CasaMagica.Ravenclaw:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Ravenclaw);
                    break;
                case CasaMagica.Slytherin:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Slytherin);
                    break;
                default:
                    solicitud.CasaString = Convert.ToString(CasaMagica.Gryffindor);

                    break;

            }




            _context.Solicitud.Add(solicitud);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSolicitud", new { id = solicitud.Id }, solicitud);
        }

        // DELETE: api/Solicituds/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSolicitud([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var solicitud = await _context.Solicitud.SingleOrDefaultAsync(m => m.Id == id);
            if (solicitud == null)
            {
                return NotFound();
            }

            _context.Solicitud.Remove(solicitud);
            await _context.SaveChangesAsync();

            return Ok(solicitud);
        }

        private bool SolicitudExists(int id)
        {
            return _context.Solicitud.Any(e => e.Id == id);
        }
    }
}